import pytest

from db_utils.bulk_operations import bulk_operation


@pytest.mark.parametrize("bulk_commands, expected_output", [
    ([
         "START BULK",
         "CREATE RESOURCE person (id_PK,name,surname);",
         "ADD TO person (id,203) (name,Max) (surname,verstappen);",
         "REMOVE FROM person at id=203",
         "CREATE RESOURCE car (id_PK,make,model);",
         "ADD TO car (id,101) (make,Mercedes-Benz) (model,w204 C63 V8);",
         "END BULK"
     ], ["Started bulk operation", "Created resource:person in the database",
         "Added row to resource:person", "Remove operation finished at resource:person",
         "Created resource:car in the database", "Added row to resource:car",
         "Ended bulk operation with no errors"]),

])
def test_bulk_operation(bulk_commands, expected_output):
    database = {}

    output = bulk_operation(database, bulk_commands)
    assert output == expected_output
