import pytest

from db_utils.create_resource import create_resource
from utils.command import db_command_switcher

TEST_COMMANDS_CREATE = [
    ("CREATE RESOURCE person(id_PK,name,surname,age);", "Created resource:person in the database"),
    ("CREATE RESOURCE car(id_PK,make,model,engine_size);", "Created resource:car in the database"),
    ("CREATE RESOURCE school(id_PK,name,numberOfStudents);", "Created resource:school in the database"),
    ("CREATE RESOURCE person(id,name,surname,age);",
     "Invalid operation: There should be exactly one primary key column."),
    ("CREATE RESOURCE school(id_PK,name,name);", "Invalid operation: Resource columns cannot have duplicate names.")
]


@pytest.mark.parametrize("input_val, expected_output", TEST_COMMANDS_CREATE)
def test_create_resources(input_val, expected_output):
    db = {}
    output = create_resource(db, input_val)
    assert output == expected_output


TEST_COMMANDS_ADD = [
    ("CREATE RESOURCE person(id_PK,name,surname);", None),
    ("CREATE RESOURCE car(id_PK,make,model,engine_size);", None),
    ("ADD TO person (id,1) (name,John) (surname,Smith);", "Added row to resource:person"),
    ("ADD TO person (name,Jane) (surname,Doe);", "Added row to resource:person"),
    ("ADD TO car (make,Toyota) (model,Camry) (engine_size,2.5);", "Added row to resource:car"),
    ("ADD TO car (make,Ford) (model,Focus);", "Added row to resource:car"),
    ("ADD TO car_db (make,Mercedes Benz) (model,C Class);", "Invalid operation: Resource does not exist.")
]
TEST_DATABASE = {}


@pytest.mark.parametrize("input_val, expected_output", TEST_COMMANDS_ADD)
def test_add_resource(input_val, expected_output):
    output = db_command_switcher(input_val, TEST_DATABASE)
    if not expected_output:
        return
    assert output == expected_output


TEST_COMMANDS_REMOVE = [
    ("CREATE RESOURCE people(id_PK,name,surname);", None),
    ("ADD TO people (id,1) (name,John) (surname,Smith);", None),
    ("REMOVE FROM people at id=1;", "Remove operation finished at resource:people"),
    ("REMOVE FROM cars at make=Toyota;", 'Invalid operation: Resource does not exist.'),
    ("REMOVE FROM people at age=25;", "Invalid operation: Condition column does not exist in the resource."),
    ("REMOVE FROM sch at name=John;", "Invalid operation: Resource does not exist.")
]


@pytest.mark.parametrize("input_val, expected_output", TEST_COMMANDS_REMOVE)
def test_remove_resource(input_val, expected_output):
    output = db_command_switcher(input_val, TEST_DATABASE)
    if not expected_output:
        return
    assert output == expected_output


TEST_COMMANDS_SEARCH = [
    ("CREATE RESOURCE person (id_PK,name,surname);", None),
    ("ADD TO person (id,2) (name,John) (surname,Smith);", "Added row to resource:person"),
    ("SEARCH person with name=John", "id:2 name:John surname:Smith|"),
    ("ADD TO person (name,John) (surname,Carrey);", "Added row to resource:person"),
    ("SEARCH person with name=John", "id:2 name:John surname:Smith|id:3 name:John surname:Carrey|"),
    ("SEARCH person with name=ZII", "No results")
]


@pytest.mark.parametrize("input_val, expected_output", TEST_COMMANDS_REMOVE)
def test_search_resource(input_val, expected_output):
    output = db_command_switcher(input_val, TEST_DATABASE)
    if not expected_output:
        return
    assert output == expected_output
