from db_utils.add_resource import add_to_resource
from db_utils.create_resource import create_resource
from db_utils.remove_resource import remove_from_resource
from db_utils.search_resource import search_resource


def db_command_switcher(command, database):
    if command.startswith("CREATE RESOURCE"):
        return create_resource(database, command)
    elif command.startswith("ADD TO"):
        return add_to_resource(database, command)
    elif command.startswith("REMOVE FROM"):
        return remove_from_resource(database, command)
    elif command.startswith("SEARCH"):
        return search_resource(database, command)
    else:
        raise Exception("The command does not exist")
