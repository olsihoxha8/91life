def parse_add_command(command):
    command_parts = command.split('(')
    resource_name = command_parts[0].strip().split()[-1]
    column_value_list = command_parts[1].split(')')[:-1]
    return resource_name, column_value_list


def parse_create_command(command):
    command_parts = command.split('(')
    resource_name = command_parts[0].strip().split()[-1]
    column_list = command_parts[1].split(')')[0]
    columns = column_list.split(',')
    return resource_name, columns


def parse_remove_command(command):
    command_parts = command.split('at')
    resource_name = command_parts[0].split()[-1].strip()
    cond_par = command_parts[1].split('=')
    return resource_name, cond_par


def parse_search_command(command):
    command_parts = command.split(' with ')
    resource_name = command_parts[0].split()[1].strip()

    cond_pair = command_parts[1].split('=')
    cond_col = cond_pair[0].strip()
    cond_val = cond_pair[1].strip()

    return resource_name, cond_col, cond_val


def get_revert_command(database, command, result):
    if command.startswith("CREATE RESOURCE"):
        resource_name = result.split()[1]
        return f"REMOVE RESOURCE {resource_name}"
    elif command.startswith("ADD TO"):
        resource_name = command.split()[2]
        pk_column = database[resource_name]["pk_column"]
        pk_value = result.split()[-1]
        return f"REMOVE FROM {resource_name} at {pk_column}={pk_value}"
    elif command.startswith("REMOVE FROM"):
        resource_name, condition = parse_remove_command(command)
        col_value_pairs = [col.strip().split('=') for col in condition.split("and")]
        revert_cols = " ".join(f"({col},{val})" for col, val in col_value_pairs)
        return f"ADD TO {resource_name} {revert_cols}"
    else:
        raise Exception("Internal error, there is no revert for the specified command")
