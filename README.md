### **Getting Started**

These instructions will get you a copy of the project up and
running on your local machine for development and testing purposes.

#### Prerequisites
* Python 3.x
* pip
* git

#### Installing
1. Clone the repository

       git clone https://gitlab.com/olsihoxha8/91life.git
2. Change into the project directory
   
       cd 91life
  
3. Install the required packages

       pip install -r requirements.txt