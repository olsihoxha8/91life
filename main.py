"""Here you can do some testing with the written code in terminal"""

from utils.command import db_command_switcher


def main():
    print("Enter command (press Enter on an empty line to quit):")
    database = {}

    while True:
        user_input = input("> ")

        if user_input.strip() == '':
            break
        output = db_command_switcher(user_input, database)
        print(output)


if __name__ == "__main__":
    main()
