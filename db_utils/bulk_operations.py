from db_utils.db_state import db_state
from utils.command import db_command_switcher
from utils.tools import get_revert_command


def bulk_operation(database, commands):
    inside_ind = False
    bulk_changes = []
    output = []

    for command in commands:
        if command == "START BULK":
            if inside_ind:
                output.append("Invalid operation: A bulk operation is already in progress.")
                return output

            inside_ind = True
            bulk_changes.clear()
            output.append("Started bulk operation")

        elif command == "END BULK":
            if not inside_ind:
                output.append("Invalid operation: No bulk operation in progress.")
                return output

            inside_ind = False
            if bulk_changes:
                bulk_changes.clear()
                output.append("Ended bulk operation with no errors")
            else:
                output.append("Ended bulk operation with errors. Rollback applied")

        elif command == "DB_STATE":
            db_state(database)

        elif inside_ind:
            result = db_command_switcher(command, database)
            output.append(result)
            if not result.startswith("Invalid operation"):
                bulk_changes.append((command, result))
            else:
                for com, res in bulk_changes:
                    revert_command = get_revert_command(database, com, res)
                    db_command_switcher(revert_command, database)
                return output

    return output


