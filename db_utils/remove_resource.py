from utils import tools


def remove_from_resource(database, command):
    resource_name, cond_par = tools.parse_remove_command(command)

    if resource_name not in database:
        return "Invalid operation: Resource does not exist."

    condition_column = cond_par[0].strip()
    condition_value = cond_par[1].strip()

    if condition_column not in database[resource_name]:
        return "Invalid operation: Condition column does not exist in the resource."
    rows_to_remove = []

    for row in database[resource_name]["rows"]:
        if row[condition_column] == condition_value:
            rows_to_remove.append(row)

    for row in rows_to_remove:
        database[resource_name]["rows"].remove(row)

    return f"Remove operation finished at resource:{resource_name}"
