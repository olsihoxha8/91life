from utils import tools


def add_to_resource(database, command):
    resource_name, column_value_list = tools.parse_add_command(command)
    column_values = [pair.strip().split(',') for pair in column_value_list]

    if resource_name not in database:
        return "Invalid operation: Resource does not exist."

    pk_column = None
    for col_name, is_pk in database[resource_name].items():
        if is_pk:
            pk_column = col_name
            break

    if not pk_column:
        return "Invalid operation: Primary key column is missing for the resource."

    pk_value_provided = any(col == pk_column for col, value in column_values)

    if not database[resource_name].get("rows"):
        database[resource_name]["rows"] = []

        if not pk_value_provided:
            column_values.append((pk_column, "1"))

    if pk_value_provided:
        for col, value in column_values:
            if col == pk_column and int(value) <= database[resource_name]["current_pk"]:
                return "Invalid operation: Provided PK value is not higher than existing PK."

    else:
        next_pk = database[resource_name]["current_pk"] + 1
        column_values.append((pk_column, str(next_pk)))

    database[resource_name]["current_pk"] = int(column_values[-1][1])

    new_row = {col: value for col, value in column_values}
    database[resource_name]["rows"].append(new_row)

    return f"Added row to resource:{resource_name}"
