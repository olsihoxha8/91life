from utils import tools


def search_resource(database, command):
    resource_name, cond_col, cond_val = tools.parse_search_command(command)

    if resource_name not in database:
        return "Invalid operation: Resource does not exist."

    if cond_col not in database[resource_name]:
        return "Invalid operation: Column does not exist in the resource."

    result_rows = []
    for row in database[resource_name]["rows"]:
        if row.get(cond_col) == cond_val:
            result_rows.append(row)

    result_rows.sort(key=lambda x: int(x["id"]))

    output = ""
    for row in result_rows:
        for col, value in row.items():
            output += f"{col}:{value} "
        output += "|"

    return output if output else "No results"
