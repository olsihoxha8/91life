def db_state(database):
    sorted_resources = sorted(database.keys())
    for resource_name in sorted_resources:
        resource = database[resource_name]
        rows = resource.get("rows", [])
        print(f"Resource:{resource_name} {len(rows)} rows")
        for row in rows:
            row_str = " ".join(f"{col}:{value}" for col, value in row.items())
            print(row_str)
