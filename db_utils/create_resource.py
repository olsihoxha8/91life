from utils import tools


def create_resource(database, command):
    resource_name, columns = tools.parse_create_command(command)

    if resource_name in database:
        return "Invalid operation: Resource name already exists."

    column_dict = {}
    for column in columns:
        column = column.strip()
        if column.endswith('_PK'):
            column_name = column[:-3]
            pk_column = True
        else:
            column_name = column
            pk_column = False

        if column_name in column_dict:
            return "Invalid operation: Resource columns cannot have duplicate names."

        column_dict[column_name] = pk_column

    pk_count = sum(column_dict.values())
    if pk_count != 1:
        return "Invalid operation: There should be exactly one primary key column."

    database[resource_name] = column_dict
    database[resource_name]["current_pk"] = 0
    database[resource_name]["rows"] = []
    return f"Created resource:{resource_name} in the database"


def remove_resource(database, command):
    resource_name = tools.parse_create_command(command)

    if resource_name not in database:
        return f"Invalid operation: Resource {resource_name} does not exist."

    del database[resource_name]
    return f"Removed resource:{resource_name} from the database"
